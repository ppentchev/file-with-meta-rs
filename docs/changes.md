<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the file-with-meta project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2025-01-22

### INCOMPATIBLE CHANGES

- All the functions accept `Utf8Path` references from the `camino` crate now,
  instead of `std::path::Path` ones as before.

### Fixes

- Bump the `serde` version requirement so that `cargo build -Zminimal-versions` works.

### Additions

- Start some `MkDocs` documentation.
- Add a Tox test framework for checking the SPDX tags.

### Other changes

- Switch to yearless copyright notices.
- Switch to SPDX copyright and license tags.
- Define MSRV 1.62, use inline format arguments and `Option::then_some()`.
- Minor refactoring and doc fixes following some suggestions from Clippy.
- Use `serde_derive` explicitly.
- Allow `thiserror` 2.x.
- Convert this changelog file to the "Keep a Changelog" format.

## [0.2.0] - 2022-11-08

### INCOMPATIBLE CHANGES

- Define a `file_with_meta::Error` enum and
  use it as the error return type instead of `Box<dyn error::Error>`.
- Also return `Result` from the `mtime_to_unix()`,
  `match_meta()`, `match_meta_with_source()`, and `build_req()`
  functions.
- Mark the `FileHttpMetadata` struct as non-exhaustive.

### Additions

- Add a test for the `build_req`() function.
- Add a `run-clippy.sh` tool for verbose diagnostics.
- Keep the `Cargo.lock` file under version control.

### Other changes

- Declare compatibility with the Rust 2021 edition.
- Lots of minor refactoring following suggestions by Clippy.
- Use `anyhow` instead of `expect-exit` in the test suite.

## [0.1.1] - 2021-09-01

### Fixes

- Turn the `html_root_url` doc attribute into a crate-level one.
- Honor the clippy tool's `needless_borrow` suggestions.

## [0.1.0] - 2021-06-22

### Started

- First public release.

[Unreleased]: https://gitlab.com/ppentchev/file-with-meta-rs/-/compare/0.3.0...main
[0.3.0]: https://gitlab.com/ppentchev/file-with-meta-rs/-/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/ppentchev/file-with-meta-rs/-/compare/0.1.1...0.2.0
[0.1.1]: https://gitlab.com/ppentchev/file-with-meta-rs/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/ppentchev/file-with-meta-rs/-/tags/0.1.0
