<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# file-with-meta: store a file's metadata for caching purposes

\[[Home][ringlet-home] | [GitLab][gitlab] | [crates.io][crates-io] | [ReadTheDocs][readthedocs]\]

## Overview

The `FileHttpMetadata` structure may be serialized and
stored in a JSON file alongside the real file, e.g. one with
".meta" appended to the file name. Then either the `match_meta`
function may be used directly, or the `build_req` one may be
used to modify an HTTP request, adding the necessary headers to
make sure that the file is not downloaded if there have been
no changes on the remote server.

The `match_meta_with_source` function may be used to additionally
make sure that a "source" file has not been modified since this file
was last generated from its data.

## Examples

Example for checking whether a file needs to be downloaded:

``` rust
  use std::fs;

  let dst = destdir.join("data.json");
  let dst_meta = destdir.join("data.json.meta");
  let (req, stored_meta) = file_with_meta::build_req(
      agent.get("https://example.com/"),
      &dst,
      &dst_meta,
  )?;
  let resp = req.call()?;
  match resp.status() {
      304 => println!("Nothing was fetched"),
      _ => {
          println!("Storing the content");
          /* ... */

          println!("Updating the file's metadata");
          let meta = file_with_meta::FileHttpMetadata::from_file(&dst)?;
          fs::write(&dst_meta, serde_json::to_string(&meta).unwrap())?;
      }
  };
```

Example for checking whether a file has changed since its metadata
was last updated:

``` rust
  let dst = "/path/to/file.dat";
  let dst_meta = "/path/to/file.dat.meta";

  match file_with_meta::match_meta(&dst, &dst_meta)?.is_some() {
      true => println!("No change"),
      false => println!("Somebody touched our file, recreate it?"),
  };
```

## Contact

The `file-with-meta` library was written by [Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab]. This documentation is
hosted at [Ringlet][ringlet-home] with a copy at [ReadTheDocs][readthedocs].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/ppentchev/file-with-meta-rs "The file-with-meta GitLab repository"
[crates-io]: https://crates.io/crates/file-with-meta "The file-with-meta crate on crates.io"
[readthedocs]: https://file-with-meta-rs.readthedocs.io/ "The file-with-meta ReadTheDocs page"
[ringlet-home]: https://devel.ringlet.net/devel/file-with-meta-rs/ "The Ringlet file-with-meta homepage"
